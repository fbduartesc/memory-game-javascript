<div align="center">
  <img alt="Memory game" src="img/memory.png" height="400px" />    
  <h1>HTML5 | CSS3 | Javascript</h1>

 :rocket: *Memory game: this project will build a game using javascript, HTML5 e CSS3.*
  </div>


# :video_game: Game demo
Demo: [Memory game](https://fbduartesc.gitlab.io/memory-game-javascript)



# :pushpin: Table of contents

- [Technologies](#computer-technologies)
- [How to run](#construction_worker-how-to-run)
- [License](#closed_book-license)

# :computer: Technologies

This project was made using the following technologies:

<ul>
  <li><a href="https://www.w3schools.com/html/">HTML5</a></li>
  <li><a href="https://developer.mozilla.org/pt-BR/docs/Web/CSS">CSS3</a></li>
  <li><a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript">Javascript</a></li>
</ul>

# :construction_worker: How to run

### :computer: Downloading project 

```bash
# Clone repository into your machine
$ git clone https://gitlab.com/fbduartesc/memory-game-javascript.git
```

### 💻 Running project on a web browser

```bash
# Open the file index.html in your browser
$ index.html
```

# :closed_book: License

Released in 2020.

Made with passion by [Fabio Duarte de Souza](https://gitlab.com/fbduartesc) 🚀.
This project is under the [MIT license](https://gitlab.com/fbduartesc/memory-game-javascript/blob/master/LICENSE).